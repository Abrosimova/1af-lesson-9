import { appOptions } from './settings.js'
import { API } from './api.js'

var Activity = {
  constructor: Activity,
  get: function(callback) {
    var that = this;

    API.getKeyFromDB({ key: appOptions.provider + '-activity-user_' + API.model.user.id_str }, function(data) {
//       data = null;
      if (data) {
        that.model = JSON.parse(data.Value);
        if (that.model.last === void 0) {
          that.model.last = 0
        }
      } else {
        that.model = {
          last: new Date().getTime(),
          invite: [],
          lessons: []
        };
      }
      if (typeof callback === 'function') {
        callback(data === null);
      }
    });
  },
  
  getLessonById: function(id) {
    id = id|0;
    for (var i = 0; i < this.model.lessons.length; i++) {
      if (this.model.lessons[i].id === id) {
        return this.model.lessons[i]
      }
    }
    
    return {
      id: id,
      video: 0,
      task: 0
    }
  },
  
  setLesson: function(lesson) {
    for (var i = 0; i < this.model.lessons.length; i++) {
      if (this.model.lessons[i].id === lesson.id) {
        this.model.lessons[i] = lesson
        
        return
      }
    }
    this.model.lessons.push(lesson);
  },
  
  updateDate: function() {
    this.model.last = new Date().getTime();
  },

  set: function(callback) {
    var that = this;
    
    this.updateDate();
    API.addKeyToDB({
      label: appOptions.provider + '-activity',
      key: appOptions.provider + '-activity-user_' + API.model.user.id_str,
      value: JSON.stringify(that.model)
    }, function() {
      if (typeof callback === 'function') {
        callback();
      }
    });
  }
}

export { Activity }
