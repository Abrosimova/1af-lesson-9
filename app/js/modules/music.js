import { appOptions } from './settings.js'

var audioPlayer = {
  init: function () {
    var player = document.getElementById('audio-player');

    if (localStorage) {
      var state = localStorage.getItem(appOptions.provider + '_music-state');

      if (state === null || state === 'on') {
        $('.music').removeClass('music--off').addClass('music--on')
        player.play();
      }
    } else {
      player.play();
      $('.music').removeClass('music--off').addClass('music--on')
    }
    $('.music').on('click', function() {
      if (!player) {
        return
      }
      if ($(this).hasClass('music--off')) {
        $(this).removeClass('music--off').addClass('music--on');
        player.play();
        if (localStorage) {
          localStorage.setItem(appOptions.provider + '_music-state', 'on');
        }
      } else if ($(this).hasClass('music--on')) {
        $(this).removeClass('music--on').addClass('music--off');
        player.pause();
        if (localStorage) {
          localStorage.setItem(appOptions.provider + '_music-state', 'off');
        }
      }
    })
  }
}

export { audioPlayer }
