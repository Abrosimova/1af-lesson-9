import { shuffleArray } from './common.js'
import { appOptions } from './settings.js'

var Content = {
  model: [
    {
      id: 1,
      img: appOptions.cdnPath + 'k1.png',
      name: 'Сидеть'
    },
    
    {
      id: 2,
      img: appOptions.cdnPath + 'k2.png',
      name: 'Лежать'
    },
    
    {
      id: 3,
      img: appOptions.cdnPath + 'k3.png',
      name: 'Ко мне'
    },
    
    {
      id: 4,
      img: appOptions.cdnPath + 'k4.png',
      name: 'Стоять'
    },
    
    {
      id: 5,
      img: appOptions.cdnPath + 'k5.png',
      name: 'Рядом'
    },
    
    {
      id: 6,
      img: appOptions.cdnPath + 'k6.png',
      name: 'Нельзя'
    },
    
    {
      id: 7,
      img: appOptions.cdnPath + 'k7.png',
      name: 'Вокруг'
    },
    
    {
      id: 8,
      img: appOptions.cdnPath + 'k8.png',
      name: 'Поклон'
    },
    
    {
      id: 9,
      img: appOptions.cdnPath + 'k9.png',
      name: 'Служить'
    },
    
    {
      id: 10,
      img: appOptions.cdnPath + 'k10.png',
      name: 'Переворот'
    }
  ],

  results: [
    {
      id: 1,
      title: 'Поздравляем! А теперь за дело, обучи собаку всему, чему сейчас научился.',
      done: 1
    },

    {
      id: 2,
      title: 'Ты не успел найти %COUNT%. Можешь приступать к тренировкам, но лучше попробуй пройти игру.',
      done: 0
    }
  ],

  getResult: function(done) {
    done = done|0;
    for (var i = 0; i < this.results.length; i++) {
      if (this.results[i].done === done ) {
        return this.results[i]
      }
    }

    return this.results[1];
  },

  getCards: function () {
    var cards = shuffleArray(this.model).slice(0, 6);
    
    cards = cards.concat(cards);
    
    return shuffleArray(cards);
  }
}

export { Content }
