/* global ga */

var Analytics = {
  initMain: function () {
    this.initCommon();
    $('.main-btn').on('click', function () {
      window.location.href = 'https://ad.dnevnik.ru/promo/1af-lessons';
      ga('send', 'event', 'Start course', 'Start course from main', 'main screen 01');
    });

    $('.video-button').on('click', function () {
      $('.fullscreen-button').click();
      ga('send', 'event', 'Video teaser start', 'Video teaser start', 'main');
    })

    $('.start-course').on('click', function () {
      window.location.href = 'https://ad.dnevnik.ru/promo/1af-lessons';
      ga('send', 'event', 'Start course', 'Start course from main', 'main screen 3');
    });

    $('.parents__btn').on('click', function () {
      window.location.href = 'https://ad.dnevnik.ru/promo/1af-about';
      ga('send', 'event', 'About', 'About from main', 'main screen 4');
    })
  }, 
  
  initLogin: function() {
    ga('send', 'event', 'Authorize prompt', 'Authorize prompt', 'authorization')
    $('.login').on('click', function () {
      ga('send', 'event', 'Authorize prompt enter', 'Authorize prompt enter', 'authorization')
    })

    $('.auth-anon').on('click', function () {
      ga('send', 'event', 'Authorize prompt decline', 'Authorize prompt decline', 'authorization')
    })
  }, 
  
  initLessons: function() {
    this.initCommon();
    
    $('.lesson').on('click', function (e) {
      if (!$(this).hasClass('lesson--collapse') && e.target.className.indexOf('lesson__arrow') === -1) {
        switch ($(this).data('id')|0) {
          case 0:
            ga('send', 'event', 'Lesson open', 'Lesson 00 welcome', 'lessons list');
            break;
          case 1:
            ga('send', 'event', 'Lesson open', 'Lesson 01 breeds', 'lessons list');
            break;
          case 2:
            ga('send', 'event', 'Lesson open', 'Lesson 02 traits', 'lessons list');
            break;
          case 3:
            ga('send', 'event', 'Lesson open', 'Lesson 03 language', 'lessons list');
            break;
          case 4:
            ga('send', 'event', 'Lesson open', 'Lesson 04 health', 'lessons list');
            break;
          case 5:
            ga('send', 'event', 'Lesson open', 'Lesson 05 nutrition', 'lessons list');
            break;
          case 6:
            ga('send', 'event', 'Lesson open', 'Lesson 06 flat', 'lessons list');
            break;
          case 7:
            ga('send', 'event', 'Lesson open', 'Lesson 07 walks', 'lessons list');
            break;
          case 8:
            ga('send', 'event', 'Lesson open', 'Lesson 08 discipline', 'lessons list');
            break;
          case 9:
            ga('send', 'event', 'Lesson open', 'Lesson 09 training', 'lessons list');
            break;
          case 10:
            ga('send', 'event', 'Lesson open', 'Lesson 10 more', 'lessons list');
            break;
        }
      } 
    })
    
    $('.dn-logo').on('click', function () {
      ga('send', 'event', 'Dnevnik link', 'Dnevnik link from lessons', 'lessons list')
    });
    
    $('.menu-button').on('click', function () {
      if ($(this).hasClass('menu-button--opened')) {
        return
      }
      ga('send', 'event', 'Menu call', 'Menu from lessons', 'lessons list')
    })
  },
  
  initLesson: function(id) {
    this.lessonId = id;
 
    ga('send', 'event', 'Lesson view', 'Lesson view L' + (id < 10 ? '0' + id : id), 'lesson page');
    
    $('.dn-logo').on('click', function () {
      ga('send', 'event', 'Dnevnik link', 'Dnevnik link from L' + (id < 10 ? '0' + id : id), 'lesson page')
    });
    
    $('.menu-button').on('click', function () {
      if ($(this).hasClass('menu-button--opened')) {
        return
      }
      ga('send', 'event', 'Menu call', 'Menu from L' + (id < 10 ? '0' + id : id), 'lesson page')
    })
    
    $('.nav-back').on('click', function() {
      ga('send', 'event', 'Lessons page', 'Lessons from L' + (id < 10 ? '0' + id : id), 'lesson page');
    });
    
    $('.nav__arrow-right').on('click', function() {
      ga('send', 'event', 'Skip to L' + ((id + 1) < 10 ? '0' + (id + 1) : (id + 1)), 'From L' + (id < 10 ? '0' + id : id), 'lesson page');
    })
    
    $('.nav__arrow-left').on('click', function() {
      ga('send', 'event', 'Skip to L' + ((id - 1) < 10 ? '0' + (id - 1) : (id - 1)), 'From L' + (id < 10 ? '0' + id : id), 'lesson page');
    })
    
    $('body').on('click', '.play-pause-button', function() {
      if (!$(this).hasClass('play-button')) {
        ga('send', 'event', 'Video lesson start', 'Video L' + (id < 10 ? '0' + id : id), 'lesson page');
      }
    })
    
    $('.download-button').on('click', function() {
      ga('send', 'event', 'Download lesson materials', 'Download L' + (id < 10 ? '0' + id : id) + ' materials', 'lesson page');
    })
  },
                 
  videoProgress: function(progress) {
    if (void 0 === this.lessonId) {
      return
    }
    switch (progress) {
      case 25: 
        ga('send', 'event', 'Video lesson view 25%', 'Video L' + (this.lessonId < 10 ? '0' + this.lessonId : this.lessonId) + ' view 25%', 'lesson page');
        break;
      case 50: 
        ga('send', 'event', 'Video lesson view 50%', 'Video L' + (this.lessonId < 10 ? '0' + this.lessonId : this.lessonId) + ' view 50%', 'lesson page');
        break;
      case 75: 
        ga('send', 'event', 'Video lesson view 75%', 'Video L' + (this.lessonId < 10 ? '0' + this.lessonId : this.lessonId) + ' view 75%', 'lesson page');
        break;
      case 100: 
        ga('send', 'event', 'Video lesson view 100%', 'Video L' + (this.lessonId < 10 ? '0' + this.lessonId : this.lessonId) + ' view 100%', 'lesson page');
        break;
    }
  },               

  initCommon: function () {
    $('.footer_menu').on('click', '.menu__link', function () {
      var link = $(this).prop('href');

      if (link.indexOf('privacy/pp-russian') > -1) {
        ga('send', 'event', 'Info links', 'Privacy', 'main footer')
      } else if (link.indexOf('legal/ld-russian') > -1) {
        ga('send', 'event', 'Info links', 'Legal', 'main footer')
      } else if (link.indexOf('/zadati-vopros-o-produktsii/') > -1) {
        ga('send', 'event', 'Info links', 'Feedback', 'main footer')
      } else if (link.indexOf('/site-owner.aspx') > -1) {
        ga('send', 'event', 'Info links', 'Mars', 'main footer')
      }
    });

    $('.social__item').on('click', function () {
      var className = $(this).prop('class');

      if (className.indexOf('social__item--ok') > -1) {
        ga('send', 'event', 'Share site', 'Share site OK', 'main footer')
      } else if (className.indexOf('social__item--fb') > -1) {
        ga('send', 'event', 'Share site', 'Share site FB', 'main footer')
      } else if (className.indexOf('social__item--vk') > -1) {
        ga('send', 'event', 'Share site', 'Share site VK', 'main footer')
      }
    })
    
    $('.menu-container').on('click', '.menu__link', function () {
      if ($(this).prop('href').indexOf('lessons') > -1) {
        ga('send', 'event', 'Lessons page', 'Lessons from menu', 'menu')
      } else if ($(this).prop('href').indexOf('about') > -1) {
        ga('send', 'event', 'About page', 'About from menu', 'menu')         
      } else if ($(this).prop('href').indexOf('document') > -1) {
        ga('send', 'event', 'Diploma page', 'Diploma from menu', 'menu')         
      } else {
        ga('send', 'event', 'Main page', 'Main from menu', 'menu')   
      }
    })
  }
}

export { Analytics }
