import { Timer } from './timer.js'
import { Analytics } from './analytics.js'
import { appOptions } from './settings.js'
import { Activity } from './activity.js'
import { App } from './../main.js'

var videoPlayer = {
  counter: -1,
  fullscreen: 0,
  prevVolume: 0,
  soundHide: 0,

  init: function () {
    this.player = document.getElementById('video-player');
    this.timer = new Timer();
    var that = this;

    if (!this.player) {
      return
    }
    var playerHeight = $(this.player).height();
    var playerWidth = playerHeight * 1.75;

    if (playerWidth > document.documentElement.clientWidth - 40) {
      playerWidth = document.documentElement.clientWidth - 40;
      playerHeight = playerWidth * 0.56;
    }

    var html = '<video class="lesson-player" id="video-player"'

    html += 'src="' + $('.lesson-player').prop('src') + '"'
    html += ' preload="true"'
    html += 'style="width:' + playerWidth + 'px; height:' + playerHeight + 'px">'
    html += 'Sorry, your browser doesn\'t support embedded videos,'
    html += 'but don\'t worry, you can <a href="video.mp4">download it</a>'
    html += 'and watch it with your favorite video player!'
    html += '</video>'
    $(this.player).replaceWith(html);
    this.player = document.getElementById('video-player');

    $('.sound-range').val(this.player.volume);

    var onFullscreenChange = function() {
      if ($('.lesson-player').width() !== document.documentElement.clientWidth) {
        $('body').children('.player-controls').remove()
      }
    }

    this.player.addEventListener('webkitfullscreenchange', onFullscreenChange);
    this.player.addEventListener('mozfullscreenchange', onFullscreenChange);
    this.player.addEventListener('fullscreenchange', onFullscreenChange);

    $('body').on('click', '.play-pause-button', function() {
      if ($('.music--on').length > 0) {
        $('.music--on').click();
      }
      $(this).toggleClass('play-button pause-button');
      if (!that.player.paused) {
        that.player.pause();
        that.stopProgress();
      } else {
        that.player.play();
        that.updateProgress();
      }
    });

    $('body').on('click', '.progress', function(e) {
      var x = e.clientX,
        y = e.clientY,
        progress = document.querySelector('.progress').getBoundingClientRect();
      var fullscreenProgress = $('body').children('.player-controls').find('.progress')
      var progressTop = fullscreenProgress.length > 0 ? fullscreenProgress.offset().top + 14 : progress.top + 10
      var progressBottom = fullscreenProgress.length > 0 ? fullscreenProgress.offset().top + fullscreenProgress.height() + 14 : progress.bottom - 10
      var progressLeft = fullscreenProgress.length > 0 ? fullscreenProgress.offset().left + 21 : progress.left + 21
      var progressRight = fullscreenProgress.length > 0 ? fullscreenProgress.offset().left + fullscreenProgress.width() - 64 : progress.right - 64

      if (y >= progressTop && y <= progressBottom && x >= progressLeft && x <= progressRight) {
        that.player.currentTime = that.player.duration * (x - progressLeft) / (progress.width - 64 - 21);
      }
    })

    $('body').on('click', '.fullscreen-button', function() {
      if ($('.lesson-player').width() !== document.documentElement.clientWidth) {
        if (!appOptions.isMobile) {
          var html = document.querySelector('.player-controls').outerHTML;

          $('body').append(html);
          $('body').children('.player-controls').css({
            'z-index': '9999999999',
            'bottom': '14px',
            'justify-content': 'center'
          })
        }

        $('.js-sound-button, .sound-range').hover(
          function() {
            $('.sound-range').css('display', 'block');
            clearInterval(that.soundHide);
          },
          function () {
            that.soundHide = setTimeout(function() {
              $('.sound-range').css('display', 'none');
            }, 1000)
          }
        )

        if (that.player.requestFullscreen) {
          that.player.requestFullscreen();
        } else if (that.player.mozRequestFullScreen) {
          that.player.mozRequestFullScreen();
        } else if (that.player.webkitRequestFullscreen) {
          that.player.webkitRequestFullscreen();
        }
      } else {
        if (document.webkitExitFullscreen) {
          document.webkitExitFullscreen();
        } else if (document.mozCancelFullscreen) {
          document.mozCancelFullscreen();
        } else if (document.exitFullscreen) {
          document.exitFullscreen();
        }
      }

    })

    $('body').on('input', '.sound-range', function() {
      var val = parseFloat($(this).val());

      this.prevVolume = that.player.volume;
      that.player.volume = val;
      if (val === 0) {
        $('.sound-button').toggleClass('sound-button sound-button--mutted')
      } else if (this.prevVolume === 0) {
        $('.sound-button--mutted').toggleClass('sound-button sound-button--mutted')
      }
    })

    $('.js-sound-button, .sound-range').hover(
      function() {
        $('.sound-range').css('display', 'block');
        clearInterval(that.soundHide);
      },
      function () {
        that.soundHide = setTimeout(function() {
          $('.sound-range').css('display', 'none');
        }, 1000)
      }
    )
  },

  stopProgress: function () {
    clearInterval(this.counter);
  },

  updateProgress: function () {
    var that = this;

    this.counter = setInterval(function() {
      var progress = parseInt((that.player.currentTime * 100) / that.player.duration, 10);

      $('.progress__line').css('width', progress + '%');
      $('.progress__timer').html(that.timer.formatTime(parseInt(that.player.currentTime, 10)))
      Analytics.videoProgress(progress);
      if (progress === 100) {
        that.stopProgress();
        $('.play-pause-button').toggleClass('play-button pause-button');
        Activity.get(function() {
          var lesson = Activity.getLessonById(App.id);

          if (lesson.video === 0) {
            lesson.video = 1;
            Activity.setLesson(lesson);
            Activity.set(function() {
              App.drawElements();  
            });
          }
        })
      }
    })

  }
}

export { videoPlayer }
