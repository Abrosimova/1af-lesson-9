import { Modal } from './modal.js'
import { Content } from './content.js'

var Test = {
  init: function(callback) {
    this.questionCounter = -1;
    this.score = 0;
    this.callback = callback;
    this.drawRulesList();
  },
  
  drawRulesList: function() {
    var html = '',
      that = this;
    
    html += '<div class="test-content"><div class="test">'
    html += '<div class="test-flex-container">'
    
    html += '<div class="test-flex__left">'
    html += '<div class="question-container">Выбери 5 вещей, которые собаке нельзя делать:</div>'
    html += '<div class="answers-container">'
    for (var i = 0; i < Content.rules.length; i++) {
      html += '<div class="answer">'
      html += '<div class="answer__radio">'
      html += '<input type="checkbox" id="check' + i + '" data-val="' + Content.rules[i].id + '"'
      html += ' name="check" class="radio-button"'
      html += i === 0 ? ' checked="checked"' : ''
      html += '/>'
      html += '<label for="check' + i + '"></label>'
      html += '</div>'
      html += '<div class="answer__label">' + Content.rules[i].text + '</div>'
      html += '</div>'
    }
    html += '</div>'
    html += '</div>'

    html += '</div>'
    html += '</div>'
    html += '<button class="pedegree-btn start-test-button start-test-button--disabled">НАЧАТЬ</button>'
    html += '</div>'
    
    $('.test-content').replaceWith(html);
    
    $('.start-test-button').on('click', function() {
      if ($(this).hasClass('start-test-button--disabled')) {
        return
      }
      var rulesIds = [];
      
      $('.radio-button:checked').each(function() {
        rulesIds.push($(this).data('val')|0);
      })
      that.model = Content.getQuetionsList(rulesIds)
      that.start()
    })
    
    $('.radio-button').on('click', function() {
      if ($('.radio-button:checked').length > 5) {
        $(this).prop('checked', false);
      }
    })
    
    $('.radio-button').on('change', function() {
      if ($('.radio-button:checked').length > 4 && $('.start-test-button').hasClass('start-test-button--disabled')) {
        $('.start-test-button').removeClass('start-test-button--disabled')
      } else if ($('.radio-button:checked').length < 5 && !$('.start-test-button').hasClass('start-test-button--disabled')) {
        $('.start-test-button').addClass('start-test-button--disabled')
      }
    })
    
    $('.test-content').on('click', '.answer__label', function() {
      $(this).prev('.answer__radio').children('input').first().click()
    })
  },

  start: function () {
    var html = '',
      that = this;

    html += '<div class="bullits-container">'
    for (var i = 0; i < this.model.length; i++) {
      html += '<div class="bullet'
      html += i === 0 ? ' bullet--active' : ''
      html += '" data-id="' + this.model[i].id + '"></div>'
    }
    html += '</div>'
//    html += '<div class="result__score"><span class="red">' + this.selected + '</span>/3</div>'
    html += '<div class="test">'
    html += '<div class="test-flex-container">'
    
    html += '<div class="test-flex__left">'
    html += '<div class="question-container"></div>'
    html += '<div class="answers-container">'
    html += '</div>'
    html += '</div>'
    
//    html += '<div class="test-flex__right">'
//    html += '<img class="question-img">'
//    html += '</div>'

    html += '</div>'
    html += '</div>'
    html += '<button class="pedegree-btn answer-button">ДАЛЕЕ</button>'
    
    $('.test-content').empty().html(html);

    $('html, body').animate({
      scrollTop: $('.test-container').offset().top + 50
    }, 500);

    $('.test-content').on('click', '.answer-button', function () {
      if ($(this).hasClass('checked')) {
        return;
      }
      $('.answer-button').addClass('checked');
      var qId = $('.question-container').data('id');
      var answerId = $('.radio-button:checked').data('val');
      var correct = that.check(qId, answerId);
      
      if (!correct) {
        var html = '';

  //      html += '<div class="question__res-title'
  //      if (correct) {
  //        html += ' question__res-title--right"> ПРАВИЛЬНО!'
  //      } else {
  //        html += ' question__res-title--wrong"> НЕПРАВИЛЬНО'
  //      }
  //      html += '</div>'
        html += '<div class="question__res-desc">'
        html += '<div class="question__res-title question__res-title--wrong">'
        html += 'Нет никаких поблажек!</div> Нужно следовать правилам, которые ты сам выбрал!</div>'
        // html += '<button class="pedegree-btn close-popup">Закрыть</button>'
        Modal.open(html);

        $('.modal-close').on('click', function() {
          that.drawNextQuestion();
        })
      } else {
        that.drawNextQuestion();
      }
    });

    $('.test-content').on('click', '.answer__label', function() {
      $(this).prev('.answer__radio').children('input').first().click()
    })

    this.drawNextQuestion();
  },

  drawNextQuestion: function() {
    var question;

    this.questionCounter++;
    if (this.questionCounter > this.model.length - 1) {
      if (typeof this.callback === 'function') {
        this.callback(this.score);

        return;
      }
    }

    $('.bullet').removeClass('bullet--active').removeClass('bullet--done')
    for (var i = 0; i < this.model.length; i++) {
      if (i < this.questionCounter) {
        $('.bullet[data-id="' + this.model[i].id + '"]').addClass('bullet--done')
      } else if (i === this.questionCounter) {
        $('.bullet[data-id="' + this.model[i].id + '"]').addClass('bullet--active')
      }
    }
//    $('.red').html(this.selected);

    question = this.model[this.questionCounter];

//    $('.question-img').prop('src', question.img);
    $('.question-container').html(question.text).data('id', question.id);
    var html = '';
    
    html += '<div class="answers-container">'
    for (i = 0; i < question.answers.length; i++) {
      html += '<div class="answer">'
      html += '<div class="answer__radio">'
      html += '<input type="radio" id="check' + i + '" data-val="' + i + '"'
      html += ' name="check" class="radio-button"'
      html += i === 0 ? ' checked="checked"' : ''
      html += '/>'
      html += '<label for="check' + i + '"></label>'
      html += '</div>'
      html += '<div class="answer__label">' + question.answers[i] + '</div>'
      html += '</div>'
    }
    html += '</div>'

    // $('.test').empty().html(html);
    $('.answers-container').html(html)
    $('.answer-button').removeClass('checked');
  },

  check: function (question, answer) {
    var q = this.getQuestionById(question);

    answer = answer|0;
    if (answer === q.correct) {
      this.score++;
    }

    return answer === q.correct;
  },

  // getScore: function() {
  //   var index = 1,
  //     max = this.score[1];
  //
  //   for (var i in this.score) {
  //     if (!this.score.hasOwnProperty(i)) {
  //       continue;
  //     }
  //     if (max < this.score[i]) {
  //       max = this.score[i];
  //       index = i;
  //     }
  //   }
  //
  //   return index;
  // },

  getQuestionById: function(id) {
    var data = this.model;

    for (var i = 0; i < data.length; i++) {
      if (data[i].id == id) {
        return data[i]
      }
    }

    return {};
  }
}

export { Test }
