import { Content } from '../content.js'
import { Timer } from '../timer.js'

var Game = {
  init: function(callback) {
    var that = this;
    var timerOptions = {
      maxTime: 60,
      reverse: 1,
      container: '.timer__time',
      callback: function () {
        that.stop();
      }
    }
    
    this.model = Content.getCards();
    this.callback = callback;
    this.score = 0;
    
    this.timer = new Timer(timerOptions);
    
    this.start();
    this.timer.start();
  },
  
  start: function() {
    var html = '',
      that = this;
    
    html += '<div class="timer"><div class="timer__time"></div></div>'
    html += '<div class="card-container">'
    for (var i = 0; i < this.model.length; i++) {
      html += '<div class="card flip-container" data-id="' + this.model[i].id + '">'
      html += '<div class="flipper">'
      html += '<div class="front"></div>'
      
      html += '<div class="back" style="background-image:url(' + this.model[i].img + ')">'
      html += '<div class="card-back__title">' + this.model[i].name + '</div>'
      html += '</div>'
      
      html += '</div>'
      html += '</div>'
    }
    html += '</div>'
    $('.test-container').empty().html(html);
    
    $('.card').on('click', function() {
      if ($(this).hasClass('disabled')) {
        return
      }
      
      $(this).addClass('card--flipped');
      if ($('.card--flipped').length === 1) {
        var el = $(this);
        
        that.flipTimer = setTimeout(function() {
          el.removeClass('card--flipped')
        }, 3000)
      } else if ($('.card--flipped').length === 2) {
        $('.card').addClass('disabled')
        clearTimeout(that.flipTimer);
        var id1 = $('.card--flipped').eq(0).data('id'),
          id2 = $('.card--flipped').eq(1).data('id');
        
        setTimeout(function() {
          if (id1 === id2) {
            $('.card--flipped').addClass('card--checked');
            that.score++;
            if ($('.card--checked').length === that.model.length) {
              that.stop();
            }
          }
          setTimeout(function() {
            $('.card--flipped').removeClass('card--flipped')
            $('.card').removeClass('disabled')
          }, 600)
        }, 600)
        
      }
    })
  },
  
  stop: function() {
    this.timer.callback = null;
    this.timer.stop();
    var score = {};
    
    score.time = this.timer.maxTime - this.timer.getTime();
    score.score = this.score;
    score.left = this.model.length / 2 - this.score;
    score.done = this.model.length / 2 === this.score ? 1 : 0;
    this.callback(score);
  }
}

export { Game }