import { svg } from './svgString.js'

var Menu = {
  init: function() {
    $('.menu-button').on('click', function() {
      if ($('.menu-container').css('display') === 'block') {
        $('.menu-container').css('display', 'none');
        $(this).removeClass('menu-button--opened')
      } else {
        $('.menu-container').css('display', 'block');
        $(this).addClass('menu-button--opened')
      }
    });  

    $('.menu__item-text').hover(function() {
      $(this).append(svg);
    },
    function() {
      $(this).find('svg').remove();
    })
  }
}

export { Menu }
