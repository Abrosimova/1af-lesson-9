import { appOptions } from './settings.js'

var Loader = {
  show: function() {
    var html = '<div class="loader"><img class="loader__img" src="' + appOptions.cdnPath + 'loader.gif"/></div>';

    $('body').append(html);
  },

  hide: function() {
    $('.loader').remove();
  }
}

export { Loader }
