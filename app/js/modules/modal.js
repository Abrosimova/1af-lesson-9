import { appOptions } from './settings.js'

var Modal = {
  overflow: '',
  open: function(content, callback) {
    var html = '',
      that = this;

    html += '<div class="overlay">'
    html += '<div class="modal">'
    html += '<img class="modal-close" src="' + appOptions.cdnPathMain + 'close-popup.png' + '">'
    html += '<div class="modal-content">'
    html += content + '</div>'
    html += '</div>'
    html += '</div>'
    this.close();
    $('body').append(html);
//    that.overflow = $('body').css('overflow');
//    $('body').css('overflow', 'hidden');
    this.setCenter();
    $(window).on('resize', this.setCenter);

    if (typeof callback === 'function') {
      callback();
    }

    $('.modal-close').on('click', function() {
      that.close();
    });
  },

  setCenter: function () {
    var h = $('.modal').innerHeight(),
      w = $('.modal').innerWidth();

    $('.modal').css('margin', '-' + Math.floor(h / 2) + 'px ' + '0 0 -' + Math.floor(w / 2) + 'px');
  },

  close: function() {
    $('.overlay').remove();
//    $('body').css('overflow', this.overflow);
  }
}

export { Modal }
