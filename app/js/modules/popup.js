/* global ga */

import { Activity } from './activity.js'
import { Modal } from './modal.js'
import { appOptions } from './settings.js'
import { shuffleArray } from './common.js'
import { API } from './api.js'

var Popup = {
  init: function() {
    var that = this;
    
    Activity.get(function(newUser) {
      var counter = 0;
      var today = new Date().setHours(0, 0, 0, 0);
      
      if (Activity.model.last < today && !newUser) {
        that.openComeback();
        Activity.updateDate();
        Activity.set();
      }
      
      for (var i = 0; i < Activity.model.lessons.length; i++) {
        if (Activity.model.lessons[i].video === 1 && Activity.model.lessons[i].task === 1) {
          counter++;
        }
      }
      if (counter === 5) {
        that.openHalfWay();
        
        return
      }
    })
    
    $(document).on('mousemove', function() {
      clearInterval(that.watcher);
      that.runWatcher();
    })
    
    that.runWatcher();
  },
  
  runWatcher: function() {
    var that = this;
    
    this.watcher = setTimeout(function() {
      if ($('.popup-container').length === 0) {
        that.openWaking();
      }
    }, 480000) 
  },
  
  openWaking: function() {
    var html = '';
    
    html += '<div class="popup-container">'
    html += '<img class="popup__cooper-img" width="287" height="467" src="' + appOptions.cdnPath + 'cooper.png"/>'
    html += '<div class="popup-text-container">'
    html += '<img class="popup__cooper-msg" width="252" height="159" src="' + appOptions.cdnPath + 'cooper-msg.png"/>'
    html += '<div class="popup-text popup-text--cooper">Это Купер. Ты не закончил задание!</div>'
    html += '<button class="pedegree-btn half__btn">Продолжим!</button>'
    html += '</div>'
    html += '</div>'
    Modal.open(html);
    
    ga('send', 'event', 'Heads up', 'Heads up show', 'popup');
    
    $('.half__btn').on('click', function() {
      ga('send', 'event', 'Heads up', 'Heads up close', 'popup');
      Modal.close();
    })
  },
  
  openHalfWay: function() {
    var html = '';
    
    html += '<div class="popup-text">Ты молодец — полпути пройдено! Держи лапу!</div>'
    html += '<img class="popup__half-img" width="297" height="223" src="' + appOptions.cdnPath + 'half.png"/>'
    html += '<button class="pedegree-btn half__btn">Пригласить друзей</button>'
    Modal.open(html);
    
    ga('send', 'event', 'Project 50% done', 'Project 50% done', 'popup');
    
    $('.half__btn').on('click', function() {
      ga('send', 'event', 'Invite friends', 'Invite friends', 'popup');
    })
  },
  
  openComeback: function() {
    var html = '',
      popupText = [
        { 
          text: 'Ура, ' + API.model.user.firstName + ' снова с нами! Продолжим!',
          button: 'Супер!'
        },
        {
          text: 'Привет-привет, ' + API.model.user.firstName + '! Продолжим!',
          button: 'Привет!'
        }
      ];
    var content = shuffleArray(popupText)[0];
    
    html += '<div class="popup-text">' + content.text + '</div>'
    html += '<img class="popup__half-img" width="546" height="211" src="' + appOptions.cdnPath + 'popup-comeback.png"/>'
    html += '<button class="pedegree-btn go-on__btn">' + content.button + '</button>'
    Modal.open(html);
    
    $('.go-on__btn').on('click', function() {
      Modal.close();
    })
  },
  
  detectIE: function() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf('MSIE ');
    var trident = ua.indexOf('Trident/');

    if (msie > 0 || trident > 0) {
      var html = '';

      html += '<div class="ie-overlay">'
      html += '<div class="ie-modal">'
      html += '<img class="close-ie-modal" src="' + appOptions.cdnPathMain + 'close-ie.png">'
      html += '<div class="ie-title">Дорогой друг!</div>'
      html += '<div class="ie-text">Наш сайт работает корректно только в браузерах:'
      html += '<br>Googlе Chrome, Mozilla Firefox, Opera.</div>'
      html += '<div class="browsers-list">'
      html += '<img class="browser" src="' + appOptions.cdnPathMain + 'chrome.png">'
      html += '<img class="browser" src="' + appOptions.cdnPathMain + 'mozilla.png">'
      html += '<img class="browser" src="' + appOptions.cdnPathMain + 'opera.png">'
      html += '</div>'
      html += '<div class="ie-text">Чтобы посмотреть видео, тебе нужно<br> открыть другой браузер.</div>'
      html += '</div>'
      html += '</div>'
      $('body').append(html);

      $('.close-ie-modal').on('click', function() {
        $('.ie-overlay').remove();
      })
    }
  }
}

export { Popup }