var Share = {
  vk: function(purl, ptitle, pimg, ptext) {
    var url = 'http://vkontakte.ru/share.php?';

    url += 'url=' + encodeURIComponent(purl);
    url += '&title=' + encodeURIComponent(ptitle);
    url += '&comment=' + encodeURIComponent(ptext);
    url += '&image=' + encodeURIComponent(pimg);
    this.popup(url);
  },

  ok: function(purl, ptitle, pimg, ptext) {
    var url = 'https://connect.ok.ru/offer?url=' + encodeURIComponent(purl);

    // url += '&st.comments=' + encodeURIComponent(text);
    // url += '&st._surl=' + encodeURIComponent(purl);
    this.popup(url);
    
    return [ptext, ptitle]
  },

  fb: function(purl, ptitle, pimg, ptext) {
    var url = 'https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(purl);

    url += '&picture=' + encodeURIComponent(pimg);
    this.popup(url);
    
    return [ptext, ptitle]
  },

  popup: function(url) {
    window.open(url, '', 'toolbar=0,status=0,width=626,height=436');
  }
}

export { Share }
