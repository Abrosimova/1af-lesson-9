import { Share } from './share.js'
import { appOptions } from './settings.js'

var Sharing = {
  init: function() {
    $('.social__item').on('click', function() {
      var method;
      var className = $(this).prop('class');
      var title = '1 «Аф» - класс для детей, которые действительно хотят собаку',
        text = 'Это первый в мире онлайн курс, пройдя который дети смогут доказать родителям, что они готовы иметь собаку и умеют за ней ухаживать. 10 интересных уроков расскажут о главном, а 10 заданий помогут закрепить знания. #pedigree #1Афкласс #собакамоямечта';

      if (className.indexOf('ok') > 0) {
        method = 'ok'
      } else if (className.indexOf('vk') > 0) {
        method = 'vk'
      } else {
        method = 'fb'
      }
      Share[method](window.location.href, title, appOptions.cdnPathMain + 'Sharing.jpg', text);
    })
  }
}

export { Sharing }