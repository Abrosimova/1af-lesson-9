import { Modal } from './modal.js'
import { appOptions } from './settings.js'
import { Auth } from './auth.js'

var Authorization = {
  init: function() {
    var html = '';
    
    if (localStorage) {
      var anon = localStorage.getItem(appOptions.provider + '_anonymus')|0;

      if (anon === 1) {
        if (appOptions.cdnPath.indexOf('lessons') > -1 || appOptions.cdnPath.indexOf('diplom') > -1) {
          html = '<div class="anon__msg">Полный курс и документ доступны только для пользователей Дневник.ру '
          html += '<img class="anon__msg-close" src="' + appOptions.cdnPathMain + 'close-popup.png"></div>';

          $('body').append(html);
          $('.anon__msg').on('click', function() {
            localStorage.removeItem(appOptions.provider + '_anonymus');
            Authorization.init();
          });
          $('.anon__msg-close').on('click', function() {
            $('.anon__msg').remove();
          });
        }
        
        return;
      }
    }
    html = '';

//    html += '<img class="modal-img auth__img" width="499" height="286" '
//    html += 'src="' + appOptions.cdnPathMain + 'login-popup.png">'
    html += '<div class="auth-container">'
    html += '<div class="auth__text">Полный <span class="auth__overline">курс и документ</span> доступны только после авторизации.</div>'
    html += '<div class="auth-actions">'
    html += '<button class="pedegree-btn login">ВОЙТИ</button>'
    html += '<div class="auth__text">или</div>'
    html += '<span class="auth-anon">Продолжить без авторизации</span>'
    html += '</div>'
    html += '</div>'
    Modal.open(html);

    $('.login').on('click', function () {
      Auth.auth();
      Modal.close();
    })

    $('.auth-anon').on('click', function () {
      if (localStorage) {
        localStorage.setItem(appOptions.provider + '_anonymus', 1);
      }
      Modal.close();
    })
  }
}

export { Authorization }
