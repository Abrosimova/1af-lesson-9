/* global ga */

import { Menu } from './modules/menu.js'
import { audioPlayer } from './modules/music.js'
import { videoPlayer } from './modules/video.js'
import { Analytics } from './modules/analytics.js'
import { Sharing } from './modules/sharing.js'
import { Auth } from './modules/auth.js'
import { Cookie } from './modules/cookie.js'
import { Authorization } from './modules/authorization.js'
import { Activity } from './modules/activity.js'
import { API } from './modules/api.js'
import { appOptions } from './modules/settings.js'
import { Game } from './modules/game/game.js'
import { Content } from './modules/content.js'
import { wordForm } from './modules/common.js'
import { Popup } from './modules/popup.js'

var App = {
  id: 9,
  
  init: function() {
    var that = this;

    Sharing.init();
    Menu.init();
    audioPlayer.init();
    videoPlayer.init();
    Analytics.initLesson(this.id);
    Popup.detectIE();
    
    API.init();
    
    if (!appOptions.isMobile && document.location.href.indexOf('access_token') > -1) {
      that.drawAuthPage();
      Auth.auth();
      window.opener.location.reload();
      window.close();
    } else if (document.location.href.indexOf('access_token') > -1) {
      Auth.auth(function() {
        API.token = Auth.token;
        API.getUserInfo();
        var path = window.location.pathname.substring(0, window.location.pathname.length);

        history.pushState('', document.title, path);
        window.location.reload();
      });
    } else {
      this.checkSession();
    }

//    Game.init(that.drawResult);
    $('.test-container').on('click', '.start-test', function() {
      Game.init(that.drawResult);
      ga('send', 'event', 'Task start', 'Task start L09', 'lesson 09 task');
    })
  },
  
  drawAuthPage: function() {
    var html = '<div class="auth-container">';

    html += '<div class="auth-container__text">Это окно закроется через несколько секунд</div>'
    html += '</div>'
    $('body').empty().html(html);
  },
  
  checkSession: function functionName() {
    var that = this;

    if (void 0 !== Cookie.get(appOptions.provider + '_token')) {
      API.token = Cookie.get(appOptions.provider + '_token');
      API.getUserInfo(function() {
        Activity.get(function () {
          that.drawElements();
        })
      });
    } else {
      Authorization.init();
    } 
  },
  
  drawElements: function() {   
    var that = this;
    
    $('.test-container').css('display', 'block');
    $('.footer').css('margin-top', 'auto');
    Activity.get(function() {
      var html = ''; 
      var lesson = Activity.getLessonById(that.id);
      
      html += '<div class="fixed">'
      html += '<div class="circle '
      html += lesson.video === 1 ? 'circle-checked' : 'circle-empty'
      html += '">'
      html += lesson.video === 1 ? '<img src="https://ad.csdnevnik.ru/special/staging/1af/img/lesson1/check.png" alt="" class="checked">' : '';
      html += '</div>'
      html += '<div class="circle '
      html += lesson.task === 1 ? 'circle-checked' : 'circle-empty'
      html += '">'
      html += lesson.task === 1 ? '<img src="https://ad.csdnevnik.ru/special/staging/1af/img/lesson1/check.png" alt="" class="checked">' : '';
      html += '</div>'
      html += '</div> ' 
      
      $('.fixed').remove(); 
      $('body').append(html)
    })
  },

  drawResult: function(score) {
    var html = '';    
    var result = Content.getResult(score.done);
    
    switch (result.id) {
      case 1:
        ga('send', 'event', 'Task result L09', 'result L09 super', 'lesson 09 task');
        break;
      case 2:
        ga('send', 'event', 'Task result L09', 'result L09 fail', 'lesson 09 task');
        break;
    }
    
    if (result.id === 1) {
      Activity.get(function() {
        var lesson = Activity.getLessonById(App.id);

        if (lesson.task === 0) {
          lesson.task = 1;
          Activity.setLesson(lesson);
          Activity.set(function() {
            App.drawElements();  
          });
        }
      })
    }

    if (result.id === 2) {
      result.title = result.title.replace('%COUNT%', score.left + ' ' + wordForm.get('comand', score.left))
    }
    html += '<div class="test-content">'
    html += '<div class="result__text">Результат</div>'
//    html += '<div class="result__score"><span class="red">' + score + '</span>/5</div>'
    html += '<div class="result__text-container">'
//    html += '<img src="' + result.img + '" alt="" class="result__img">'
    html += '<div class="result__text">'
    html += '<div class="result__desc">' + result.title + '</div>'
    if (result.id === 1) {
      html += '<div class="result__desc">Ты справился за ' + score.time + ' ' + wordForm.get('second', score.time) + '</div>'
    }
    html += '</div>' 
    html += '</div>'
    // html += '<div class="result__desc">' + result.title + '</div>'
    // html += '<img src="' + result.img + '" alt="" class="result__img"'
    // html += ' width="494" height="480">'
    html += '<button class="pedegree-btn play-again">'
    html += '<span class="play-again__text">ЕЩЕ РАЗ</span>'
    html += '</button>'
    html += '<div class="next-link-container"><a href="'
    html += window.location.href.indexOf('mosreg') > -1 ? 'https://ad.school.mosreg.ru/promo/1af-lesson10' : 'https://ad.dnevnik.ru/promo/1af-lesson10'
    html += '">'
    html += '<div class="nav-next">Следующий урок</div>'
    html += '</a></div>'
    // html += '<div class="repost">'
    // html += '<div class="repost__title">Расскажи друзьям</div>'
    // html += '<ul class="repost__list">'
    // html += '<li class="repost__list-item repost__list-item--ok"></li>'
    // html += '<li class="repost__list-item repost__list-item--fb"></li>'
    // html += '<li class="repost__list-item repost__list-item--vk"></li>'
    // html += '</ul>'
    // html += '</div>'
    html += '</div>'
    $('.test-container').empty().html(html);

    $('html, body').animate({
      scrollTop: $('.test-container').offset().top + 60
    }, 500);
    
    $('.next-link-container').on('click', function() {
      ga('send', 'event', 'Next lesson', 'next lesson', 'lesson 09 task');
    })
 
    $('.play-again').on('click', function() {
      ga('send', 'event', 'Task retake', 'task retake L09', 'lesson 09 task');
      
      var html = '<div class="test__text">ЗАДАНИЕ</div>'

      html += '<div class="test__title section__title">Я обучаю собаку командам</div>'
      html += '<div class="test__task">'
      html += 'Собак нужно дрессировать и обучать командам. Во-первых, это так классно, когда кто-то приносит тапочки, во-вторых, собаке самой интересно и приятно учиться выполнять команды. Пройди игру, и после этого тебе станет намного проще обучать собаку командам'
      html += '</div>'
      // html += '<img src="https://ad.csdnevnik.ru/special/staging/1af/img/lesson1/test-img.png" alt="" class="test-bg">'
      html += '<button class="pedegree-btn start-test">НАЧАТЬ</button>' 

      $('.test-content').empty().html(html);

      $('html, body').animate({
        scrollTop: $('.test-container').offset().top - 20
      }, 500);
    })
  }
}

$(document).ready(function() {
  App.init();
})

export { App }
