var gulp = require('gulp'),
  autoprefixer = require('gulp-autoprefixer'),
  cleanCSS = require('gulp-clean-css'),
  importCss = require('gulp-import-css'),
  rollup = require('rollup'),
  jsdoc = require('gulp-jsdoc3'),
  eslint = require('gulp-eslint'),
  replace = require('gulp-replace-path');

gulp.task('doc', function(cb) {
  var config = require('./jsdoc.json');

  gulp.src(['./js/app/**/*.js'], { read: false })
  .pipe(jsdoc(config, cb));
});

gulp.task('css:minify', function() {
  gulp.src('./app/css/*.css')
  .pipe(importCss())
  .pipe(autoprefixer({
    browsers: ['last 10 versions']
  }))
  .pipe(cleanCSS())
  .pipe(gulp.dest('./dist/css/'));
});

gulp.task('js:lint', function() {
  return gulp.src(['./app/**/*.js'])
  .pipe(eslint())
  .pipe(eslint.format())
  .pipe(eslint.failAfterError());
});

gulp.task('js:build', ['js:lint'], function() {
  return rollup.rollup({
    entry: './app/js/main.js'
  }).then(function(bundle) {
    bundle.write({
      format: 'iife',
      moduleName: '1af',
      dest: './dist/js/app-lesson9.js',
      sourceMap: false
    });
  })
});

gulp.task('prod', ['js:build'], function() {
  gulp.src(['./dist/js/app.js'])
    .pipe(replace(/\.\/dist/g, 'https://ad.csdnevnik.ru/special/staging/kaspersky-chat'))
    .pipe(gulp.dest('./dist/js/'));
  gulp.src(['./index.html'])
    .pipe(replace(/\.\/dist/g, 'https://ad.csdnevnik.ru/special/staging/kaspersky-chat'))
    .pipe(gulp.dest('./dist'));
})

gulp.watch(['./app/**'], ['js:build', 'css:minify']);

gulp.task('default', ['js:build', 'css:minify']);
